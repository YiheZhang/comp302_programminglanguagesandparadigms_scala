import scala.math.sqrt
import scala.math.pow

object Q1 extends App{
  def euler(k: Int): Double = {
    //use recursion to sum numbers
    def sum(n: Int): Double = {
      if (n>0){
        (1 / pow(n, 2)) + sum(n-1)
      }
      else{
        0
      }
    }
    //return result
    sqrt(6 * sum(k))
  }
}
