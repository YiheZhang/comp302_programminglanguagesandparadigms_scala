
object Q2 extends App{
  def show(x: List[Any]): String = {
    //Base case
    if (x.length == 0){
      "Nil"
    }
    else if (x.tail == Nil){
      x.head + "::Nil)"
    }
    //get the first element and then add the returned elements
    else{
      x.head + "::(" + show(x.tail)
    }
  }

  println(show(1::2::3::Nil))
}
