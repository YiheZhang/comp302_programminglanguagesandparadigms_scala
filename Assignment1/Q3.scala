object Q3 extends App{
  def fib2(n: Int):Int = {
    //matrix exponentiation algorithm
    //a function to multiply 2 matrices
    def mul(m1: Array[Array[Int]], m2: Array[Array[Int]]): Array[Array[Int]] ={
      val m00 = m1(0)(0)*m2(0)(0) + m1(1)(0)*m2(0)(1)
      val m01 = m1(0)(1)*m2(0)(0) + m1(1)(1)*m2(0)(1)
      val m10 = m1(0)(0)*m2(1)(0) + m1(1)(0)*m2(1)(1)
      val m11 = m1(0)(1)*m2(1)(0) + m1(1)(1)*m2(1)(1)
      Array(Array(m00, m01), Array(m10, m11))
    }

    //computes the power of a matrix
    def pow(m: Array[Array[Int]], i: Int): Array[Array[Int]] = {
      //if n<0, return -1
      if (i < 0) Array(Array(-1, -1), Array(-1, -1))
      else if (i == 0) Array(Array(1, 0), Array(0, 1))
      else {
        if (i % 2 != 0) {
          mul(mul(pow(m, (i - 1) / 2), pow(m, (i - 1) / 2)), m)
        }
        else {
          mul(pow(m, i/2), pow(m, i/2))
        }
      }
    }
    pow(Array(Array(1, 1), Array(1, 0)), n)(0)(1)
  }
}
