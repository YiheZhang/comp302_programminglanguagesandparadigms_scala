
object Q4 extends App{
  def cakes(n: Int): Boolean = {
    //base case
    if(n==40){
      return true
    }
    else if(n<40){
      return false
    }
    //otherwise
    else{
      //condition 1
      if(n%2==0){
        if(cakes(n/2)){
          return true
        }
      }
      //condition 2
      if((n%3==0) || (n%4==0)){
        //y = multiply the last two digits
        val y = ((((n.toString.head).asDigit).toString).last).asDigit * (n % 10)
        //if at least one of the last two digits == 0, return false
        if (y == 0) {
          return false
        } else {
          if(cakes(n - y)){
            return true
          }
        }
      }
      //condition 3
      if(n%5==0){
        if(cakes(n-40)){
          return true
        }
      }
      return false
    }
  }
}
