import scala.io.Source

object Q5 extends App{
  def shoelace(fileName: String):Double = {

    //read the file, convert everything to string
    //remove white spaces, to double and then to array
    val numbers = Source.fromFile(fileName).mkString.split("\\s+").map(_.toDouble).toArray

    //println(numbers.mkString(" "))
    //n = size of the array/2
    val size = numbers.size/2

    //use recursion to sum numbers
    def sum1(n: Int, arr: Array[Double]): Double = {
      //base case
      if(n==2){
        arr(0)*arr(3)
      }
      else{
        arr(n*2-1)*arr(n*2-4) + sum1(n-1, arr)
      }
    }

    def sum2(n: Int, arr: Array[Double]): Double = {
      //base case
      if(n==2){
        arr(2)*arr(1)
      }
      else{
        arr(n*2-2)*arr(n*2-3) + sum2(n-1, arr)
      }
    }

    //use formula and return the result
    .5* Math.abs(sum1(size, numbers) + numbers(2*size-2)*numbers(1)
      - sum2(size, numbers) - numbers(0)*numbers(2*size-1))
  }
}
