object Q6 extends App{

  def stringPipeline(cha: String): (String => String) = {

    //convert the string to upper case
    def toUpper(s: String): String = {
      s.toUpperCase()
    }
    //convert the string to lower case
    def toLower(s: String): String = {
      s.toLowerCase()
    }
    //convert the string to title case
    def cap(s: String): String = {
      s.split(" ").map(_.capitalize).mkString(" ")
    }
    //reverse the string
    def reverse(s: String):String = {
      s.reverse
    }
    //sort the upper char
    def sort(s: String):String = {
      s.sortWith(_<_)
    }
    //delete all space
    def remove(s: String):String = {
      s.replaceAll("\\s+", "")
    }
    //find the specific command
    def find(c: Char): (String => String) = {
      c match{
        case 'U' => toUpper(_)
        case 'l' => toLower(_)
        case 'T' => cap(_)
        case 'r' => reverse(_)
        case 's' => sort(_)
        case '*' => remove(_)
      }
    }

    //base case
    if (cha.length==1){
      find(cha.head)
    }
    else{
      find(cha.head) andThen stringPipeline(cha.tail)
    }

  }
}
