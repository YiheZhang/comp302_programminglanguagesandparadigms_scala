import scala.util.parsing.combinator._

object q1b {

  class WMLParser extends RegexParsers {
    val Tstart = "\\{{2}".r
    val Tend = "\\}{2}".r
    val Vstart = "\\{3}".r
    val Vend = "\\}{3}".r
    val Dstart = "\\{\\'".r
    val Dend = "\\'\\}".r
    val Pipe = "\\|".r
    val Pipes = "\\|{2}".r

    val Outertext = "^((?!\\{{2}|\\{\\')(.|\\n))+".r
    val Inneritext = "^((?!\\{{2,3}|\\{\\'|\\|{1,2}|\\}{2})(.|\\n))+".r
    val Innerdtext = "^((?!\\{{2,3}|\\{\\'|\\|{1,2}|\\'\\})(.|\\n))+".r
    val Bodytext = "^((?!\\{{2,3}|\\{\\'\\})(.|\\n))+".r
    val Vname = "^((?!\\|{1,2}|\\}{3})(.|\\n))+".r
  }

}
