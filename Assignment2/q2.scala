
abstract class ASTNode

case class ASTProgram(val x: List[ASTNode]) extends ASTNode{
  override def toString: String = "PROGRAM (" + x.mkString("") + ")"
}
case class ASTOut(val x: String) extends ASTNode{
  override def toString: String = "OUTERTEXT"
}
case class ASTInni(val x: String) extends ASTNode{
  override def toString: String = "INNERITEXT"
}
case class ASTInnd(val x: String) extends ASTNode{
  override def toString: String = "INNERDTEXT"
}
case class ASTTargs(val x: List[ASTNode]) extends ASTNode{
  override def toString: String = "TARGS (" + x.mkString("") + ")"
}
case class ASTInvoke(val x: ASTItext, val y: ASTTargs) extends ASTNode{
  override def toString: String = "INVOKE (" + x.toString + " " + y.toString + ")"
}
case class ASTItext(val x: List[ASTNode]) extends ASTNode{
  override def toString: String = "ITEXT (" + x.mkString("") + ")"
}
case class ASTVname(val x: String) extends ASTNode{
  override def toString: String = "VNAME"
}
case class ASTTvar(val x: List[ASTNode]) extends ASTNode{
  override def toString: String = "TVAR (" + x.mkString("") + ")"
}
case class ASTDefine(val x: ASTDtextn, val y: ASTDparams, val z: ASTDtextb) extends ASTNode{
  override def toString: String = "DEFINE (" + x.toString + " " + y.toString + " " + z.toString + ")"
}
case class ASTDtextn(val x:List[ASTNode]) extends ASTNode {
  override def toString: String = "DTEXTN (" + x.mkString("") + ")"
}
case class ASTDparams(val x:List[ASTNode]) extends ASTNode {
  override def toString: String = "DPARAMS (" + x.mkString("") + ")"
}
case class ASTDtextp(val x:List[ASTNode]) extends ASTNode {
  override def toString: String = "DTEXTP (" + x.mkString("") + ")"
}
case class ASTDtextb(val x:List[ASTNode]) extends ASTNode {
  override def toString: String = "DTEXTB (" + x.mkString("") + ")"
}
case class ASTBodytext(val x: String) extends ASTNode{
  override def toString: String = "BODYTEXT"
}