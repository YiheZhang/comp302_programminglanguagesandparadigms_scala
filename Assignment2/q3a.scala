import scala.io._
import scala.util.matching.Regex
import scala.util.parsing.combinator._

object q3a{

  abstract class ASTNode

  case class ASTProgram(val x: List[ASTNode]) extends ASTNode{
    override def toString: String = "PROGRAM (" + x.mkString("") + ")"
  }
  case class ASTOut(val x: String) extends ASTNode{
    override def toString: String = "OUTERTEXT"
  }

  class WMLParser extends RegexParsers {


    val Outertext = "^((?!\\{{2}|\\{\\')(.|\\n))+".r


    def program: Parser[ASTProgram] = rep(Outertext)^^{
      x => ASTProgram(x.map(ASTOut))
    }
  }

  def main(args: Array[String]): Unit = {
    //Open a file and print the result
    val f = Source.fromFile(args(0))
    val p = new WMLParser
    val ast = p.parseAll(p.program, f.mkString)
    println(ast.get)
  }
}
