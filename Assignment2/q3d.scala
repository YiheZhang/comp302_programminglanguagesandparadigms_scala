import scala.io._
import scala.util.matching.Regex
import scala.util.parsing.combinator._

object q3d{

  //Define regular expressions
  val Tstart = "\\{\\{".r
  val Tend = "\\}\\}".r
  val Vstart = "\\{\\{\\{".r
  val Vend = "\\}\\}\\}".r
  val Dstart = "\\{\\'".r
  val Dend = "\\'\\}".r
  val Pipe = "\\|".r
  val Pipes = "\\|\\|".r

  val Outertext = "^((?!\\{{2}|\\{\\')(.|\\n))+".r
  val Inneritext = "^((?!\\{{2,3}|\\{\\'|\\|{1,2}|\\}{2})(.|\\n))+".r
  val Innerdtext = "^((?!\\{{2,3}|\\{\\'|\\|{1,2}|\\'\\})(.|\\n))+".r
  val Bodytext = "^((?!\\{{2,3}|\\{\\'\\})(.|\\n))+".r
  val Vname = "^((?!\\|{1,2}|\\}{3})(.|\\n))+".r

  //Construct an AST
  abstract class ASTNode

  case class ASTProgram(val x: List[ASTNode]) extends ASTNode{
    override def toString: String = "PROGRAM (" + x.mkString("") + ")"
  }
  case class ASTOut(val x: String) extends ASTNode{
    override def toString: String = "OUTERTEXT"
  }
  case class ASTInni(val x: String) extends ASTNode{
    override def toString: String = "INNERITEXT"
  }
  case class ASTInnd(val x: String) extends ASTNode{
    override def toString: String = "INNERDTEXT"
  }
  case class ASTTargs(val x: List[ASTNode]) extends ASTNode{
    override def toString: String = "TARGS (" + x.mkString("") + ")"
  }
  case class ASTInvoke(val x: ASTItext, val y: ASTTargs) extends ASTNode{
    override def toString: String = "INVOKE (" + x.toString + " " + y.toString + ")"
  }
  case class ASTItext(val x: List[ASTNode]) extends ASTNode{
    override def toString: String = "ITEXT (" + x.mkString("") + ")"
  }
  case class ASTVname(val x: String) extends ASTNode{
    override def toString: String = "VNAME"
  }
  case class ASTTvar(val x: List[ASTNode]) extends ASTNode{
    override def toString: String = "TVAR (" + x.mkString("") + ")"
  }
  case class ASTDefine(val x: ASTDtextn, val y: ASTDparams, val z: ASTDtextb) extends ASTNode{
    override def toString: String = "DEFINE (" + x.toString + " " + y.toString + " " + z.toString + ")"
  }
  case class ASTDtextn(val x:List[ASTNode]) extends ASTNode {
    override def toString: String = "DTEXTN (" + x.mkString("") + ")"
  }
  case class ASTDparams(val x:List[ASTNode]) extends ASTNode {
    override def toString: String = "DPARAMS (" + x.mkString("") + ")"
  }
  case class ASTDtextp(val x:List[ASTNode]) extends ASTNode {
    override def toString: String = "DTEXTP (" + x.mkString("") + ")"
  }
  case class ASTDtextb(val x:List[ASTNode]) extends ASTNode {
    override def toString: String = "DTEXTB (" + x.mkString("") + ")"
  }
  case class ASTBodytext(val x: String) extends ASTNode{
    override def toString: String = "BODYTEXT"
  }

  class WMLParser extends RegexParsers {

    //Define grammar rules

    def program: Parser[ASTProgram] = rep(Outertext ^^ ASTOut | invoke) ^^ ASTProgram

    def invoke: Parser[ASTInvoke] = Tstart ~> itext ~ targs <~ Tend ^^ {
      case x ~ y => ASTInvoke(x, y)
    }

    def targs: Parser[ASTTargs] = rep(Pipe ~> opt(itext)^^{
      case Some(x) => x
      case None => ASTItext(Nil)
    }) ^^ ASTTargs

    def itext: Parser[ASTItext] = rep(Inneritext ^^ ASTInni | tvar | define | invoke ) ^^ ASTItext

    def tvar: Parser[ASTTvar] = Vstart ~> Vname ~ opt(Pipe ~ itext) <~ Vend ^^ {
      case x ~ None => ASTTvar(List(ASTVname(x)))
      case x ~ Some(a ~ b) => ASTTvar(List(ASTVname(x), b))
    }


    def define: Parser[ASTDefine] = Dstart ~> dtextn ~ dparams ~ Pipes ~ dtextb <~ Dend ^^ {
      case x ~ y ~ a ~ z => ASTDefine(x, y, z)
    }

    def dtextn: Parser[ASTDtextn] = rep(Innerdtext ^^ ASTInnd | invoke | define | tvar) ^^ ASTDtextn

    def dparams: Parser[ASTDparams] = rep(Pipe ~> dtextp) ^^ ASTDparams

    def dtextp: Parser[ASTDtextp] = rep1(Innerdtext ^^ ASTInnd | invoke | define | tvar) ^^ ASTDtextp

    def dtextb: Parser[ASTDtextb] = rep(Bodytext ^^ ASTBodytext | invoke | define | tvar) ^^ ASTDtextb

  }

  def main(args: Array[String]): Unit = {
    //Open a file and print the result
    val f = Source.fromFile(args(0))
    val p = new WMLParser
    val ast = p.parseAll(p.program, f.mkString)
    println(ast.get)
  }
}
